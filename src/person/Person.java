package person;

public class Person {
    private int age;

    private double money;

    private boolean drunk;



    public Person(int age)
    {
        this.age = age;
    }


    public int getAge() {
        return age;
    }

    public void setDrunk(boolean drunk){
        this.drunk = drunk;
    }

    public boolean isDrunk(){
        return drunk;
    }

    @Override
    public String toString() {
        return "Person{" +
                "age=" + age +
                ", money=" + money +
                ", drunk=" + drunk +
                '}';
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }
}
